"""A easy way for you to invoke api by Bilibili. Lib 'requests' is optional.

@author KAAAsS

讲道理，写这个库的人并不会写python。
搜集了一些B站的API，当然这个库里出现的是我们收集到的部分API。出于各种各样的考量，我不打算公开所有API。
希望你能喜欢这个库~当然，赏脸的话请Star me或Fork me！大部分注释已经改成中文了，小部分没改的不是我懒得
改就是我懒得切输入法。
对了，因为我真的真的不会写python，写的渣的地方还请指正。推issue给我或者直接修正了提交pr都是no problem
的！（川普脸）
啊差点忘了，requests不是必须的。区别大致就是，如果你没有requests，那么调用API就会返回拼接完成的url。
（无论是否设置“仅返回url”）因为有可能是POST请求，所以实际返回的url格式如下：
>>>{
>>>    'method': 'POST', # or GET
>>>    'url': 'http://www.bilibili.com/xxx', # including params
>>>    'body': {'a':'b'} # only available for method 'POST'
>>>}
"""
__author__ = "KAAAsS"  # ←超帅
__version__ = '0.0.1'
__license__ = "MIT license"

from .main import *
