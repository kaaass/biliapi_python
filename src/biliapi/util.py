# 常用API地址
import biliapi

MAIN_HOST = 'http://www.bilibili.com'
API_HOST = 'https://api.bilibili.com'
ACCOUNT_HOST = 'https://account.bilibili.com/api'
APP_HOST = 'http://app.bilibili.com'
LIVE_HOST = 'http://live.bilibili.com'
GROUP_HOST = 'http://www.im9.com/api'
GROUP_ADMIN_HOST = GROUP_HOST + '/manager'
BILIAPI_HOST = 'https://api.kaaass.net/biliapi'

# cookie_domain
DOMAIN_BILI = '.bilibili.com'
DOMAIN_IM9 = '.im9.com'

# captcha_domain
CAPTCHA_IM9 = 'http://www.im9.com/web/captcha.do'
CAPTCHA_BILI = 'http://www.bilibili.com/plus/widget/ajaxGetCaptchaKey.php?js'


def sort_dict_by_key(dict):
    dict = sorted(dict.items(), key=lambda d: d[0])
    return dict


def append_param(url, param):
    if url.find('?') == -1:
        return url + '?' + param
    else:
        return url + '&' + param


def is_biliapi(obj):
    if not isinstance(obj, biliapi.main.Biliapi):
        raise biliapi.BiliAPIError('Illegal type of param biliapi.')
